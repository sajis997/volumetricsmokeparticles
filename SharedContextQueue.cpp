#include "SharedContextQueue.h"


SharedContextQueue::SharedContextQueue()
{

}

SharedContextQueue::~SharedContextQueue()
{
   
}

void SharedContextQueue::initializeSharedContext()
{
   m_context = compute::opengl_create_shared_context();

   //get the device attached to the context
   m_device = m_context.get_device();

   //get the command queue
   m_command_queue = compute::command_queue(m_context,m_device);
}

void SharedContextQueue::initializeContext()
{
   m_context = compute::system::default_context();

   //get the device attached to the context
   m_device = m_context.get_device();

   //get the command queue
   m_command_queue = compute::command_queue(m_context,m_device);   
}
