#ifndef __PARTICLESYSTEM_H__
#define __PARTICLESYSTEM_H__



#include "config.h"
#include "GpuArray.h"
#include "nvMath.h"
#include "helper_timer.h"
#include "SimParams.h"

using namespace nv;

using compute::float2_;
using compute::float4_;


class ParticleSystem
{
public:
   ParticleSystem(unsigned int numParticles, // number of particles to use
		  bool bUseVBO = true,       // use vbo or not
		  bool bUseGL = true);            // use GL or not

   ~ParticleSystem();

   enum ParticleConfig
   {
      CONFIG_RANDOM,
      CONFIG_GRID,
      _NUM_CONFIGS
   };

   void step(float deltaTime);

   void depthSort();

   void reset(ParticleConfig config);

   unsigned int getNumParticles()
   {
      return m_numParticles;
   }

   unsigned int getPosBuffer()
   {
      return m_pos->getReadVbo();
   }

   unsigned int getVelBuffer()
   {
      return m_vel->getReadVbo();
   }

   unsigned int getColorBuffer()
   {
      return 0;
   }

   unsigned int getSortedIndexBuffer()
   {
      return (unsigned int)(m_indices->getReadVbo());
   }

   const std::vector<unsigned int>& getSortedIndices();

   float getParticleRadius()
   {
      return m_particleRadius;
   }

   //get the reference to the simulation parameters
   SimParams &getParams()
   {
      return m_params;
   }

   void setSorting(bool x)
   {
      m_doDepthSort = x;
   }

   void setModelView(float *m);

   void setSortVector(compute::float4_ v)
   {
      m_sortVector = v;
   }

   void addSphere(unsigned int& index,
		  vec3f pos,
		  vec3f vel,
		  int r,
		  float spacing,
		  float jitter,
		  float lifetime);

   void discEmitter(unsigned int& index,
		    vec3f pos,
		    vec3f vel,
		    vec3f vx,
		    vec3f vy,
		    float r,
		    int n,
		    float lifetime,
		    float lifetimeVariance);

   void sphereEmitter(unsigned int& index,
		      vec3f pos,
		      vec3f vel,
		      vec3f spread,
		      float r,
		      int n,
		      float lifetime,
		      float lifetimeVariance);
   
   void dumpParticles(unsigned int start,unsigned int count);
   void dumpBin(compute::float4_ *posData, compute::float4_ *velData);

protected: //methods

   ParticleSystem() {}

   void _initialize(int numParticles,bool bUseGL);
   void _free();

   void createNoiseTexture(int w,int h, int d);

   void buildIntegrationProgram();
   
   void integrateSystem(const compute::buffer &oldPositionBuffer,
			const compute::buffer &newPositionBuffer,
			const compute::buffer &oldVelocityBuffer,
			const compute::buffer &newVelocityBuffer,
			float deltaTime,unsigned int numParticles);

   void calcDepth(const compute::buffer &positionBuffer,
		  const compute::buffer &sortKeysBuffer,
		  const compute::buffer &indicesBuffer);

   void sortParticles(const compute::buffer &sortKeysBuffer,
		      const compute::buffer &indicesBuffer);
   
   void initGrid(vec3f start,
		 compute::int4_ size,
		 vec3f spacing,
		 float jitter,
		 vec3f vel,
		 unsigned int numParticles,
		 float lifetime=100.0f);

   void initCubeRandom(vec3f origin,vec3f size,vec3f vel,float lifetime=100.0f);

protected:

   //holds value - if the particle system is initialized or not
   bool m_bInitialized;
   bool m_bUseVBO;
   
   unsigned int m_numParticles;

   float m_particleRadius;

   GpuArray<compute::float4_> *m_pos;
   GpuArray<compute::float4_> *m_vel;


   GpuArray<compute::float_> *m_sortKeys;
   GpuArray<compute::uint_> *m_indices;
   
   //params
   SimParams m_params;

   float4x4 m_modelView;
   
   compute::float4_ m_sortVector;

   bool m_doDepthSort;


   StopWatchInterface *m_timer;
   float m_time;

   //variable to contain the noise texture
   compute::image3d m_noiseImage;

   //declare a generic buffer to contain the simulation parameters
   compute::buffer m_simulationBuffer;

   compute::program m_integrationProgram;
   compute::kernel m_integrationKernel;
};


#endif // __PARTICLESYSTEM_H__
