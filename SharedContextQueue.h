#ifndef SHARED_CONTEXT_QUEUE_H
#define SHARED_CONTEXT_QUEUE_H
#include <boost/compute/system.hpp>
#include <boost/compute/interop/opengl.hpp>
#include <boost/compute/command_queue.hpp>

namespace compute = boost::compute;

class SharedContextQueue
{
public:
   SharedContextQueue();
   ~SharedContextQueue();

   void initializeSharedContext();
   void initializeContext();

public:
   compute::device m_device;
   compute::context m_context;
   compute::command_queue m_command_queue;   
};

#endif
