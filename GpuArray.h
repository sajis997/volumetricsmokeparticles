#ifndef GPU_ARRAY_H
#define GPU_ARRAY_H


#include <iostream>
#include <vector>

using namespace std;

#include "config.h"
#include "SharedContextQueue.h"
#include "singleton.h"

namespace compute = boost::compute;


template<class T>
class GpuArray
{
public:
   
   //constructor and destructor
   GpuArray();
   ~GpuArray();
   
   //enum to mention
   //the direction of the copy operation
   enum Direction
   {
      HOST_TO_DEVICE,
      DEVICE_TO_HOST,
   };

   //allocate memory on the host and device
   //with/without cl-gl interoperability
   void alloc(size_t size,
	      bool vbo = false,
	      bool doubleBuffer=false,
	      bool useElementArray=false);
   
   //deallocate memory
   void free();
   
   //swap buffers for double buffering
   void swap();
   
   // when using vbo, must map before getting device buffer
   // with the cl-gl interoperability
   void map();
   void unmap();
   
   //copy the buffer between the host and the device
   void copy(Direction dir, uint start=0, uint count=0);
      
   //get the current read and write buffer of the device
   const compute::buffer& getReadBuffer() const;
   const compute::buffer& getWriteBuffer() const;
   
   //get the host buffer
   std::vector<T>& getHostBuffer();

   GLuint getReadVbo() const;
   GLuint getWriteVbo() const;
   
   //get the size of the bufffer
   size_t getSize() const;
   
private:
   
   GLuint createVbo(size_t size, bool useElementArray);
   
   void allocDevice();
   void allocVbo(bool useElementArray);
   void allocHost();
   
   void freeDevice();
   void freeVbo();
   void freeHost();

   //number of elements - not in bytes !!!
   size_t m_size;
   
   //identifier to the vertex buffer object
   GLuint m_vbo[2];
   
   //declare a vector on the device to hold two
   //opengl buffer - one as the front buffer and
   //the other as the back buffer
   std::vector<compute::opengl_buffer> m_glBuffer;
   
   //vector of vectors on the device
   std::vector<compute::vector<T> > m_genericBuffer;
   
   //std vector of host buffer
   std::vector<T> m_hostBuffer;

   //flag to use the vbo or not
   bool m_useVBO;
   //flag to use the double buffer or not
   bool m_doubleBuffer;

   //flag that mentions if the resource is locked
   //before its usage
   bool m_sharedSingleResourceAcquired;
   bool m_sharedDoubleResourceAcquired;

   //index to the read buffer or the write buffer
   unsigned int m_currentRead, m_currentWrite;   
   
};


template<class T>
GpuArray<T>::GpuArray() :
   m_size(0),
   m_currentWrite(0),
   m_currentRead(0),
   m_sharedSingleResourceAcquired(false), // no shared resource acquired yet
   m_sharedDoubleResourceAcquired(false)
{
   //initialize the vbo identifier
   m_vbo[0] = 0;
   m_vbo[1] = 0;

   //clear the host buffer
   m_hostBuffer.clear();

   //clear the device buffer
   m_genericBuffer.clear();

   //clear the device buffer with the gl interoperability
   m_glBuffer.clear();
}

template<class T>
GpuArray<T>::~GpuArray()
{
   //free the buffer
   free();
}

template<class T>
void GpuArray<T>::alloc(size_t size,bool vbo,bool doubleBuffer,bool useElementArray)
{
   //set the number of particles in this case
   m_size = size;

   //set the flag to use vbo/double-buffer or not
   m_useVBO = vbo;
   m_doubleBuffer = doubleBuffer;

   if(m_doubleBuffer)
   {
      m_currentWrite = 1;
   }

   //allocate host memory
   allocHost();

   if (vbo)
   {
      allocVbo(useElementArray);
   }
   else
   {
      //allocate in the generic device if no VBO is used
      //probably if no gl interop is found
      allocDevice();
   }   
}


template<class T>
void GpuArray<T>::free()
{
   freeHost();

   if(m_vbo)
   {
      freeVbo();
   }

   freeDevice();
}



template <class T>
void GpuArray<T>::allocHost()
{
   //pre-allocate the host buffer
   m_hostBuffer.resize(m_size);
}

template <class T>
void GpuArray<T>::freeHost()
{
   m_hostBuffer.clear();
}


template <class T>
void GpuArray<T>::allocDevice()
{
   //get the pointer to the singleton context
   SharedContextQueue *context = Singleton<SharedContextQueue>::getPtr();

   //CHECKED - NO ASSERTION IS NECESSARY
   //assert(context != NULL);
   
   //declare a device buffer
   compute::vector<T> singleBuffer(m_size,context->m_context);

   //m_genericBuffer is the std vector
   //and it contains the device buffer

   //insert the device buffer inside the
   //std buffer
   m_genericBuffer.push_back(singleBuffer);


   if (m_doubleBuffer)
   {
      //double buffer is enabled
      compute::vector<T> doubleBuffer(m_size,context->m_context);

      m_genericBuffer.push_back(doubleBuffer);
   }
   
}


template <class T>
void GpuArray<T>::freeDevice()
{
   for(unsigned int i = 0; i < m_genericBuffer.size(); ++i)
   {
      m_genericBuffer[i].clear();
   }

   m_genericBuffer.clear();
}


template <class T>
GLuint GpuArray<T>::createVbo(size_t size, bool useElementArray)
{
   GLuint vbo;

   //generate a buffer object

   //generate name for the buffer
   glGenBuffers(1, &vbo);
   
   if (useElementArray)
   {
      //now bind it to the context using the GL_ARRAY_BUFFER binding point
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo);

      //allocate storage space
      //now specify the amount of storage we want to use for the buffer
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, 0, GL_DYNAMIC_DRAW);

      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
   }
   else
   {
      glBindBuffer(GL_ARRAY_BUFFER, vbo);
      glBufferData(GL_ARRAY_BUFFER, size, 0, GL_DYNAMIC_DRAW);
      glBindBuffer(GL_ARRAY_BUFFER, 0);
   }
   
   return vbo;
}


template <class T>
void GpuArray<T>::allocVbo(bool useElementArray)
{
   //get the pointer to the singleton context
   SharedContextQueue *context = Singleton<SharedContextQueue>::getPtr();
   
   
   //create vertex buffer object
   m_vbo[0] = createVbo(m_size*sizeof(T), useElementArray);
   

   //initiate an opencl buffer from the opengl buffer object(vbo)
   //will issue error from the boost compute if unsuccessful to allocate
   compute::opengl_buffer singleBuffer(context->m_context,m_vbo[0]);

   //insert the single buffer into the gl buffer vector
   m_glBuffer.push_back(singleBuffer);   
   
   
   if (m_doubleBuffer)
   {
      //if double buffer is requested

      //create another verte buffer object
      m_vbo[1] = createVbo(m_size*sizeof(T), useElementArray);

      compute::opengl_buffer doubleBuffer(context->m_context,m_vbo[1]);

      m_glBuffer.push_back(doubleBuffer);
   }
}


template <class T>
void GpuArray<T>::freeVbo()
{
   //get the pointer to the singleton context
   SharedContextQueue *context = Singleton<SharedContextQueue>::getPtr();
   
   if (m_vbo[0])
   {
      if(m_sharedSingleResourceAcquired)
      {
	 opengl_enqueue_release_buffer(m_glBuffer[0],context->m_command_queue);
	 m_sharedSingleResourceAcquired = false;
      }
      glDeleteBuffers(1, &m_vbo[0]);
      m_vbo[0] = 0;
   }

   if (m_vbo[1])
   {
      if(m_sharedDoubleResourceAcquired)
      {
	 opengl_enqueue_release_buffer(m_glBuffer[1],context->m_command_queue);
	 m_sharedDoubleResourceAcquired = false;
      }
      glDeleteBuffers(1, &m_vbo[1]);
      m_vbo[1] = 0;
   }

   //make sure that the command queue has finished execution
   context->m_command_queue.finish();
   
   //also clear the std vector for the device with the gl-cl interoperability
   m_glBuffer.clear();
}


template <class T>
void GpuArray<T>::swap()
{
   std::swap(m_currentRead, m_currentWrite);
}

template <class T>
void GpuArray<T>::map()
{
   //get the pointer to the singleton context
   SharedContextQueue *context = Singleton<SharedContextQueue>::getPtr();

   //block untill all GL execution is complete
   glFinish();

   
   if (m_vbo[0])
   {
      if(!m_sharedSingleResourceAcquired)
      {
	 opengl_enqueue_acquire_buffer(m_glBuffer[m_currentRead],context->m_command_queue);
	 m_sharedSingleResourceAcquired = true;
      }
   }
   
   if (m_doubleBuffer && m_vbo[1])
   {
      if(!m_sharedDoubleResourceAcquired)
      {
	 opengl_enqueue_acquire_buffer(m_glBuffer[m_currentWrite],context->m_command_queue);
	 m_sharedDoubleResourceAcquired = true;
      }
   }
}


template <class T>
void GpuArray<T>::unmap()
{
   //get the pointer to the singleton context
   SharedContextQueue *context = Singleton<SharedContextQueue>::getPtr();
      
   if (m_vbo[0])
   {
      if(m_sharedSingleResourceAcquired)
      {
	 opengl_enqueue_release_buffer(m_glBuffer[m_currentRead],context->m_command_queue);
	 m_sharedSingleResourceAcquired = false;
      }
   }

   if (m_vbo[1])
   {
      if(m_sharedDoubleResourceAcquired)
      {
	 opengl_enqueue_release_buffer(m_glBuffer[m_currentWrite],context->m_command_queue);
	 m_sharedDoubleResourceAcquired = false;
      }
   }

   context->m_command_queue.finish();
      
}

//get the current read and write buffer
template <class T>
const compute::buffer& GpuArray<T>::getReadBuffer() const 
{   
   if(m_useVBO)
   {
      return m_glBuffer[m_currentRead];
   }
   else
   {
      return m_genericBuffer[m_currentRead].get_buffer();
   }
}

template <class T>
const compute::buffer& GpuArray<T>::getWriteBuffer() const
{
   if(m_useVBO)
   {
      return m_glBuffer[m_currentWrite];
   }
   else
   {
      return m_genericBuffer[m_currentWrite].get_buffer();
   }
}

//return a reference to the host buffer
template <class T>
std::vector<T>& GpuArray<T>::getHostBuffer()
{
   return m_hostBuffer;
}


template <class T>
GLuint GpuArray<T>::getReadVbo() const
{
   return m_vbo[m_currentRead];
}

template <class T>
GLuint GpuArray<T>::getWriteVbo() const
{
   return m_vbo[m_currentWrite];
}

template <class T>
size_t GpuArray<T>::getSize() const
{
   return m_size;
}


template <class T>
void GpuArray<T>::copy(Direction dir, unsigned int start, unsigned int count)
{
   if (count==0)
   {
      //update the default value
      count = (unsigned int) m_size;
   }

   //get the pointer to the singleton context
   SharedContextQueue *context = Singleton<SharedContextQueue>::getPtr();
   
   map();
   
   switch (dir)
   {      
      case HOST_TO_DEVICE:
      {
	 //point to the beginning of the host buffer
	 //in another way to convert stl vector to ray array
	 T *host_data_ptr = &m_hostBuffer[0];
	 context->m_command_queue.enqueue_write_buffer(getReadBuffer(),
						       start*sizeof(T),
						       count*sizeof(T),
						       (void*)(host_data_ptr+start));
      }	 
      break;
      
      case DEVICE_TO_HOST:
      {
	 T *host_data_ptr = &m_hostBuffer[0];
	 context->m_command_queue.enqueue_read_buffer(getReadBuffer(),
						      start*sizeof(T),
						      count*sizeof(T),
						      (void*)(host_data_ptr+start));	 
      }
      break;
   }
   
   unmap();
}


#endif
