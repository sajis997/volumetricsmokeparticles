#ifndef CONFIG_H
#define CONFIG_H

//inserted by Sajjad
#define BOOST_COMPUTE_DEBUG_KERNEL_COMPILATION 1

#include <boost/compute/types/builtin.hpp>
#include <boost/compute/image_format.hpp>
#include <boost/compute/image_sampler.hpp>
#include <boost/compute/image3d.hpp>
#include <boost/compute/source.hpp>
#include <boost/compute/system.hpp>
#include <boost/compute/container/vector.hpp>
#include <boost/compute/algorithm/iota.hpp>
#include <boost/compute/algorithm/fill.hpp>
#include <boost/compute/algorithm/transform.hpp>
#include <boost/compute/interop/opengl.hpp>

#include <boost/compute/iterator/zip_iterator.hpp>
#include <boost/compute/algorithm/for_each.hpp>
#include <boost/compute/algorithm/iota.hpp>
#include <boost/compute/algorithm/sort_by_key.hpp>

namespace compute = boost::compute;

#endif
