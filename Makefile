CC = g++
CFLAGS = -O2 -g
INCLUDE = -I/usr/local/cuda-5.5/include
INCLUDE += -I/home/sajjad/Downloads/OpenCL/library/BoostCompute/compute-develop/include
LDFLAGS =  -lglut -lGL -lGLU -lGLEW -lOpenCL
COBJS=$(patsubst %.cpp,%.o,$(wildcard *.cpp))

EXE= smokeparticles

all: $(COBJS) 
	$(CC) $(CFLAGS) -o $(EXE) $(COBJS) $(LDFLAGS)

%.o : %.cpp 
	$(CC) $(CFLAGS) -o $@ -c $< $(INCLUDE) 


clean:
	rm -f $(EXE) *.o *~

