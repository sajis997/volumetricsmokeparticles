#include <cassert>
#include <cmath>
#include <memory.h>
#include <cstdio>
#include <cstdlib>
#include <algorithm>

#include <GL/glew.h>

#if defined(__APPLE__) || defined(MACOSX)
#include <GLUT/glut.h>
#else
#include <GL/freeglut.h>
#endif

#include "helper_functions.h"
#include "ParticleSystem.h"
#include "SharedContextQueue.h"
#include "singleton.h"

// random float [0, 1]
inline float frand()
{
   return rand() / (float) RAND_MAX;
}

// signed random float [-1, 1]
inline float sfrand()
{
   return frand()*2.0f-1.0f;
}

// random signed vector
inline vec3f svrand()
{
   return vec3f(sfrand(), sfrand(), sfrand());
}


// random point in circle
inline vec2f randCircle()
{
   vec2f r;
   
   do
   {
      r = vec2f(sfrand(), sfrand());
   }
   while (length(r) > 1.0f);
   
   return r;
}

// random point in sphere
inline vec3f randSphere()
{
   vec3f r;
   
   do
   {
      r = svrand();
   }
   while (length(r) > 1.0f);
   
   return r;
}

ParticleSystem::ParticleSystem(uint numParticles, bool bUseVBO, bool bUseGL) :
   m_bInitialized(false),          // flag that the system is not initialized yet- it means the allocation is not done yet
   m_bUseVBO(bUseVBO),             // flag to use the vbo or not
   m_numParticles(numParticles),   // number of particles to deal with
   m_particleRadius(0.1f),         // set the particle radius
   m_doDepthSort(false),           // set the depth sort to false
   m_timer(NULL),                  // timer is yet to be initialized
   m_time(0.0f)                    // time value is set to zero
{

   m_params.gravity.x = 0.0f;
   m_params.gravity.y = 0.0f;
   m_params.gravity.z = 0.0f;
   m_params.gravity.w = 0.0f;

   m_params.globalDamping = 1.0f;

   m_params.noiseSpeed.x = 0.0f;
   m_params.noiseSpeed.y = 0.0f;
   m_params.noiseSpeed.z = 0.0f;
   m_params.noiseSpeed.w = 0.0f;
   
   //instantiate class objects  for gpu arrays
   //clear all the buffers that reside inside it
   m_pos = new GpuArray<compute::float4_>();
   m_vel = new GpuArray<compute::float4_>();

   //allocate buffer for sort keys and sort indices
   m_sortKeys = new GpuArray<compute::float_>();
   m_indices = new GpuArray<compute::uint_>();
   
   //initialize the particle system
   _initialize(numParticles, bUseGL);   
}


ParticleSystem::~ParticleSystem()
{
   _free();
   m_numParticles = 0;
}


void ParticleSystem::step(float deltaTime)
{
   assert(m_bInitialized);

   //update the simulation parameter for time
   m_params.time = m_time;

   SharedContextQueue *context = Singleton<SharedContextQueue>::getPtr();

   //whenever the simulation data is updated on the host side
   //it has to be updated on the device
   //write the updated simulation data to the device buffer
   context->m_command_queue.enqueue_write_buffer(m_simulationBuffer,(void*)&m_params);
   
   m_pos->map();
   m_vel->map();
      
   //integrate particles
   integrateSystem(m_pos->getReadBuffer(),   // return all the buffers with the 
		   m_pos->getWriteBuffer(),  // opengl interoperability
		   m_vel->getReadBuffer(),
		   m_vel->getWriteBuffer(),
		   deltaTime,
		   m_numParticles);

   m_pos->unmap();
   m_vel->unmap();

   //swap the read and write buffer for both the position and velocity
   m_pos->swap();
   m_vel->swap();

   m_time += deltaTime;
   
}

void ParticleSystem::buildIntegrationProgram()
{
   //get the pointer to the shared context
   SharedContextQueue *context = Singleton<SharedContextQueue>::getPtr();
   
   const char source[] =
      "typedef struct SimParams                                                       \n"
      "{                                                                              \n"
      "  float4  gravity;                                                             \n"
      "  float globalDamping;                                                         \n"
      "  float noiseFreq;                                                             \n"
      "  float noiseAmp;                                                              \n"
   
      "  float4 cursorPos;                                                            \n"
   
      "  float time;                                                                  \n"
      "  float4 noiseSpeed;                                                           \n"
      "} SimParams;                                                                   \n"      
      "__kernel void integrateSystem(__global const float4* oldPos,                  \n"
      "                              __global const float4* oldVel,                  \n"
      "                              __global float4* newPos,                        \n"
      "                              __global float4* newVel,                        \n"
      "                              __read_only image3d_t input_image,              \n"
      "                              __constant SimParams* params,                   \n"
      "                              float deltaTime,                                \n"
      "                              int numParticles)                               \n"
      "{                                                                             \n"
      "            int gid = get_global_id(0);                                       \n"
      "            if(gid < numParticles)                                            \n"
      "            {                                                                 \n"
      "              //get the initial positional and velocity data                  \n"
      "              float3 pos = (float3)(oldPos[gid].x,                            \n"
      "                                    oldPos[gid].y,                            \n"
      "                                    oldPos[gid].z);                           \n"
      "              float3 vel = (float3)(oldVel[gid].x,                            \n"
      "                                    oldVel[gid].y,                            \n"
      "                                    oldVel[gid].z);                           \n"
      "              //update the particle age                                       \n"
      "              float age = oldPos[gid].w;                                      \n"
      "              float lifetime = oldVel[gid].w;                                 \n"
      "              if(age < lifetime)                                              \n"
      "              {                                                               \n"
      "                 age += deltaTime;                                            \n"
      "              }                                                               \n"
      "              else                                                            \n"
      "              {                                                               \n"
      "                 age = lifetime;                                              \n"
      "              }                                                               \n"
      "              //apply accelerations                                           \n"
      "              vel += params->gravity * deltaTime;                             \n"
      "              //apply procedural noise                                        \n"
      "              const sampler_t sampler = CLK_NORMALIZED_COORDS_TRUE |          \n"
      "                                        CLK_ADDRESS_REPEAT  |                 \n"
      "                                        CLK_FILTER_LINEAR;                    \n"
      "              float3 texPos = pos * params->noiseFreq +                       \n"
      "                              params->time * params->noiseSpeed;              \n"
      "              float4 newTexPos = (float4)(texPos,0.0f);                       \n"
      "              float4 noise = read_imagef(input_image,sampler,newTexPos);      \n"
      "              float3 newNoise = (float3)(noise.x,noise.y,noise.z);            \n"
      "              vel += noise * params->noiseAmp;                                \n"
      "              pos += vel * deltaTime;                                         \n"
      "              vel *= params->globalDamping;                                   \n"
      "              newPos[gid] = (float4)(pos,age);                                \n"
      "              newVel[gid] = (float4)(vel,oldVel[gid].w);                      \n"
      "            }                                                                 \n"
      "}";

   //declare a program with the source
   m_integrationProgram = compute::program::create_with_source(source,context->m_context);

   //compile the program
   m_integrationProgram.build();

   //create the kernel
   m_integrationKernel = compute::kernel(m_integrationProgram,"integrateSystem");
}



void ParticleSystem::integrateSystem(const compute::buffer &oldPositionBuffer,
				     const compute::buffer &newPositionBuffer,
				     const compute::buffer &oldVelocityBuffer,
				     const compute::buffer &newVelocityBuffer,
				     float deltaTime,unsigned int numParticles)
{

   SharedContextQueue *context = Singleton<SharedContextQueue>::getPtr();
     
   m_integrationKernel.set_arg(0,oldPositionBuffer); // read-only position buffer
   m_integrationKernel.set_arg(1,oldVelocityBuffer); // read-only velocity buffer
   m_integrationKernel.set_arg(2,newPositionBuffer); // write-only position buffer
   m_integrationKernel.set_arg(3,newVelocityBuffer); // write-only velocity buffer
   m_integrationKernel.set_arg(4,m_noiseImage);      // noise texture
   m_integrationKernel.set_arg(5,m_simulationBuffer);// simulation parameters
   m_integrationKernel.set_arg(6,deltaTime);         // delta time
   m_integrationKernel.set_arg(7,numParticles);      // number of particles 

   
   //size_t localWorkSize = m_integrationKernel.get_work_group_info(context->m_device,CL_KERNEL_WORK_GROUP_SIZE);
   size_t localWorkSize = 512;
   size_t globalSize = numParticles;
   
   int r = numParticles % localWorkSize;

   if(r != 0)
      globalSize = globalSize + localWorkSize - r;
   
   //execute the kernel
   context->m_command_queue.enqueue_1d_range_kernel(m_integrationKernel,0,globalSize,localWorkSize);
   
}

void ParticleSystem::addSphere(unsigned int& index,
			       vec3f pos,
			       vec3f vel,
			       int r,
			       float spacing,
			       float jitter,
			       float lifetime)
{
   unsigned int start = index;
   unsigned int count = 0;

   //get the reference to the host position and velocity
   //that will be updated in the following loop
   std::vector<compute::float4_> &posHostRef = m_pos->getHostBuffer();
   std::vector<compute::float4_> &velHostRef = m_vel->getHostBuffer();   

   for(int z = -r; z <= r; z++)
   {
      for(int y = -r; y <= r; y++)
      {
	 for(int x = -r; x <= r; x++)
	 {
	    vec3f delta = vec3f((float)x,(float)y,(float)z) * spacing;
	    //get the length of the vector
	    float dist = length(delta);

	    if((dist <= spacing * r) && (index < m_numParticles))
	    {
	       vec3f p = pos + delta + svrand() * jitter;
	        	       
	       posHostRef[index] = compute::float4_(pos.x,pos.y,pos.z,0.0f);
	       velHostRef[index] = compute::float4_(vel.x,vel.y,vel.z,lifetime);

	       index++;
	       count++;
	    }
	 }
      }
   }

   //copy data from the host buffer to the device buffer
   m_pos->copy(GpuArray<compute::float4_>::HOST_TO_DEVICE,start,count);
   m_vel->copy(GpuArray<compute::float4_>::HOST_TO_DEVICE,start,count);
}

void ParticleSystem::discEmitter(unsigned int& index,
				 vec3f pos,
				 vec3f vel,
				 vec3f vx,
				 vec3f vy,
				 float r,
				 int n,
				 float lifetime,
				 float lifetimeVariance)
{

   std::vector<compute::float4_> &posHostRef = m_pos->getHostBuffer();
   std::vector<compute::float4_> &velHostRef = m_vel->getHostBuffer();
   
   unsigned int start = index;
   unsigned int count = 0;

   for(int i = 0; i < n; i++)
   {
      vec2f delta = randCircle() * r;

      if(index < m_numParticles)
      {
	 vec3f p = pos + delta.x * vx + delta.y * vy;
	 float lt = lifetime + frand() * lifetimeVariance;

	 posHostRef[index] = compute::float4_(p.x,p.y,p.z,0.0f);
	 velHostRef[index] = compute::float4_(vel.x,vel.y,vel.z,lt);

	 index++;
	 count++;
      }
   }

   //copy data from host to device
   m_pos->copy(GpuArray<compute::float4_>::HOST_TO_DEVICE,start,count);
   m_vel->copy(GpuArray<compute::float4_>::HOST_TO_DEVICE,start,count);
}


void ParticleSystem::sphereEmitter(unsigned int& index,
				   vec3f pos,
				   vec3f vel,
				   vec3f spread,
				   float r,
				   int n,
				   float lifetime,
				   float lifetimeVariance)
{
   //get the reference to the host buffer which will be populated and
   //copied to the device
   std::vector<compute::float4_> &posHostRef = m_pos->getHostBuffer();
   std::vector<compute::float4_> &velHostRef = m_vel->getHostBuffer();

   //initialize the start with the index value - contains the initial value
   //to start copy from
   unsigned int start = index;

   //number of elements to copy to/from
   unsigned int count = 0;

   for(int i = 0; i < n; i++)
   {
      //random point on the sphere
      vec3f x = randSphere();

      if(index < m_numParticles)
      {
	 vec3f p = pos + x*r;
	 float age = 0.0;
	 
	 float lt = lifetime + frand()*lifetimeVariance;
	 
	 vec3f dir = randSphere();
	 dir.y = fabs(dir.y);
	 vec3f v = vel + dir*spread;

	 //update the position and velocity value - are you really updating
	 posHostRef[index] = compute::float4_(p.x,p.y,p.z,age);
	 velHostRef[index] = compute::float4_(v.x,v.y,v.z,lt);

	 index++;
	 count++;
      }
   }
   
   //copy back the positional and velocity value back to GPU device
   m_pos->copy(GpuArray<compute::float4_>::HOST_TO_DEVICE, start, count);
   m_vel->copy(GpuArray<compute::float4_>::HOST_TO_DEVICE, start, count);

}

void ParticleSystem::dumpBin(compute::float4_ *posData, compute::float4_ *velData)
{
   m_pos->copy(GpuArray<compute::float4_>::DEVICE_TO_HOST);

   std::vector<compute::float4_> &hostRefPos = m_pos->getHostBuffer();

   posData = &hostRefPos[0];
   
   m_vel->copy(GpuArray<compute::float4_>::DEVICE_TO_HOST);

   std::vector<compute::float4_> &hostRefVel = m_vel->getHostBuffer();

   velData = &hostRefVel[0];
}


void ParticleSystem::dumpParticles(uint start, uint count)
{
   
   m_pos->copy(GpuArray<compute::float4_>::DEVICE_TO_HOST);

   const std::vector<compute::float4_> &pos = m_pos->getHostBuffer();

   
   m_vel->copy(GpuArray<compute::float4_>::DEVICE_TO_HOST);

   const std::vector<compute::float4_> &vel = m_vel->getHostBuffer();
   
   
   for (unsigned int i=start; i<start+count; i++)
   {
      printf("%d: ", i);
      printf("pos: (%.4f, %.4f, %.4f, %.4f)\n", pos[i][0], pos[i][1], pos[i][2], pos[i][3]);
      printf("vel: (%.4f, %.4f, %.4f, %.4f)\n", vel[i][0], vel[i][1], vel[i][2], vel[i][3]);
   }
   
}

//depth sort the particles
void ParticleSystem::depthSort()
{
   if(!m_doDepthSort)
   {
      return;
   }

   m_pos->map();
   m_indices->map();

   //calculate depth
   calcDepth(m_pos->getReadBuffer(),
	     m_sortKeys->getReadBuffer(),
	     m_indices->getReadBuffer());

   //sort the particles - radix sort
   sortParticles(m_sortKeys->getReadBuffer(),
		 m_indices->getReadBuffer());

   m_pos->unmap();
   m_indices->unmap();
}

void ParticleSystem::sortParticles(const compute::buffer &sortKeysBuffer,
				   const compute::buffer &indicesBuffer)
{
   //beginning of the key sequence
   compute::buffer_iterator<compute::float_> keysStart = compute::make_buffer_iterator<compute::float_>(sortKeysBuffer,0);
   //end of the key sequence
   compute::buffer_iterator<compute::float_> keysEnd = compute::make_buffer_iterator<compute::float_>(sortKeysBuffer,sortKeysBuffer.size()/sizeof(compute::float_));

   //beginning of the value sequence
   compute::buffer_iterator<compute::uint_> indicesStart = compute::make_buffer_iterator<compute::uint_>(indicesBuffer,0);

   //get the pointer to the singleton context
   SharedContextQueue *context = Singleton<SharedContextQueue>::getPtr();

   
   compute::sort_by_key(keysStart,keysEnd,indicesStart,context->m_command_queue);
}


void ParticleSystem::calcDepth(const compute::buffer &positionBuffer,
			       const compute::buffer &sortKeysBuffer,
			       const compute::buffer &indicesBuffer)
{
   
   //make iterator for each of the buffer
   compute::buffer_iterator<compute::float4_> posStart = compute::make_buffer_iterator<compute::float4_>(positionBuffer,0);
   compute::buffer_iterator<compute::float4_> posEnd = compute::make_buffer_iterator<compute::float4_>(positionBuffer,positionBuffer.size()/sizeof(compute::float4_));

   compute::buffer_iterator<compute::float_> sortKeysStart = compute::make_buffer_iterator<compute::float_>(sortKeysBuffer,0);
   compute::buffer_iterator<compute::float_> sortKeysEnd = compute::make_buffer_iterator<compute::float_>(sortKeysBuffer,sortKeysBuffer.size()/sizeof(compute::float_));

   compute::buffer_iterator<compute::uint_> indicesStart = compute::make_buffer_iterator<compute::uint_>(indicesBuffer,0);
   compute::buffer_iterator<compute::uint_> indicesEnd = compute::make_buffer_iterator<compute::uint_>(indicesBuffer,indicesBuffer.size()/sizeof(compute::uint_));   

   //get the pointer to the singleton context
   SharedContextQueue *context = Singleton<SharedContextQueue>::getPtr();

   
   
   BOOST_COMPUTE_CLOSURE(compute::float_, calcDepth_functor, (compute::float4_),(m_sortVector),
			 {
			    return -dot(_1,m_sortVector);
			 });
      

   compute::transform(posStart,posEnd,sortKeysStart,calcDepth_functor,context->m_command_queue);

   //generate sequence of values starting with 0
   compute::iota(indicesStart,indicesEnd,0,context->m_command_queue);

}

const std::vector<unsigned int>& ParticleSystem::getSortedIndices()
{
   m_indices->copy(GpuArray<unsigned int>::DEVICE_TO_HOST);
   return m_indices->getHostBuffer();
}



void ParticleSystem::_free()
{
   assert(m_bInitialized);

   //delete all the dynamic memory allocated so far
   if(m_pos)
   {
      delete m_pos;
      m_pos = NULL;
   }
   
   if(m_vel)
   {
      delete m_vel;
      m_vel = NULL;
   }

   if(m_sortKeys)
   {
      delete m_sortKeys;
      m_sortKeys = NULL;
   }

   if(m_indices)
   {
      delete m_indices;
      m_indices = NULL;
   }
}


void ParticleSystem::createNoiseTexture(int w,int h, int d)
{
   //get the pointer to the singleton context
   SharedContextQueue *context = Singleton<SharedContextQueue>::getPtr();

   //calculate the number of elements to deal with
   unsigned int elements = static_cast<unsigned int>(w * h * d);

   //allocate memory for the number of elements
   float *volumeData = (float*)malloc(elements * 4 * sizeof(float));
   //point to the volume data
   float *ptr = volumeData;

   //POPULATE WITH random values
   for(unsigned int i = 0; i < elements;i++)
   {
      *ptr++ = frand()*2.0f-1.0f;
      *ptr++ = frand()*2.0f-1.0f;
      *ptr++ = frand()*2.0f-1.0f;
      *ptr++ = frand()*2.0f-1.0f;
   }

   //create OpenCL 3D image data

   //define the image format
   compute::image_format volumeFormat(CL_RGBA,CL_FLOAT);

   //create the 3d opencl image buffer
   m_noiseImage = compute::image3d(context->m_context,
				   CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
				   volumeFormat,
				   w,
				   h,
				   d,
				   0, // row pitch - left with 0 for the optimal value
				   0, // slice pitch - left with 0 for the optimal value
				   volumeData);

   free(volumeData);

   //the sampler will be created and initialized inside the kernel
}

void ParticleSystem::setModelView(float *m)
{
   for (int i=0; i<16; i++)
   {
      m_modelView.m[i] = m[i];
   }
}


void ParticleSystem::_initialize(int numParticles,bool bUseGL)
{
   assert(!m_bInitialized);
   
   //create the opencl 3d image object and sampler
   //populate the image buffer with the random data
   createNoiseTexture(64,64,64);

   //allocate the gpu arrays for the particle position
   //BOTH PARTICLE AND VELOCITY ARRAYS HAS THE FOLLOWING CHARACTERISTICS:
   //1. uses vertex buffer object
   //2. uses the double buffer
   //3. do not use the element array
   m_pos->alloc(m_numParticles,m_bUseVBO,true); // create as vbo
   m_vel->alloc(m_numParticles,m_bUseVBO,true);

   //allocate buffer for sort keys - single buffer
   //a generic buffer , no shared memory with opengl
   //1. do not use the vertex buffer object
   //2. do not use the double buffer
   //3. do ont use the element array
   m_sortKeys->alloc(m_numParticles);

   //create as index buffer - single buffer
   //1. use the vertex buffer object
   //2. do not use the double buffer
   //3. use element array
   m_indices->alloc(m_numParticles,m_bUseVBO,false,true);

   sdkCreateTimer(&m_timer);

   SharedContextQueue *context = Singleton<SharedContextQueue>::getPtr();
   
   //allocate buffer to contain the simulation data
   m_simulationBuffer = compute::buffer(context->m_context,sizeof(SimParams));

   //write the simulation data to the device buffer
   context->m_command_queue.enqueue_write_buffer(m_simulationBuffer,(void*)&m_params);

   //build the integration kernel
   buildIntegrationProgram();
   
   //flag that the particle system is initialized
   m_bInitialized = true;
   
}

void ParticleSystem::initGrid(vec3f start,
			      compute::int4_ size,
			      vec3f spacing,
			      float jitter,
			      vec3f vel,
			      unsigned int numParticles,
			      float lifetime)
{

   //get the reference to the host position and velocity
   std::vector<compute::float4_> &posHostRef = m_pos->getHostBuffer();
   std::vector<compute::float4_> &velHostRef = m_vel->getHostBuffer();
   
   srand(1973);
   
   for (uint z=0; z < size[2]; z++)
   {
      for (uint y=0; y < size[1]; y++)
      {
	 for (uint x=0; x < size[0]; x++)
	 {
	    uint i = (z * size[2] * size[0]) + ( y * size[0]) + x;
	    
	    if (i < numParticles)
	    {
	       vec3f pos = start + spacing * vec3f((float)x, (float)y, (float)z) + svrand()*jitter;

	       compute::float4_ posData(pos.x, pos.y, pos.z, 0.0f);
	       compute::float4_ velData(vel.x, vel.y, vel.z, lifetime);

	       //populate with the positional and velocity data
	       posHostRef[i] = posData;
	       velHostRef[i] = velData;
	    }
	 }
      }
   }
}

void ParticleSystem::reset(ParticleConfig config)
{
   switch (config)
   {
      default:
      case CONFIG_RANDOM:
	 initCubeRandom(vec3f(0.0, 1.0, 0.0),
			vec3f(1.0, 1.0, 1.0),
			vec3f(0.0f),
			100.0);
	 break;
	 
      case CONFIG_GRID:
      {
	 float jitter = m_particleRadius*0.01f;
	 uint s = (int) ceilf(powf((float) m_numParticles, 1.0f / 3.0f));
	 uint gridSize[3];
	 gridSize[0] = gridSize[1] = gridSize[2] = s;
	 initGrid(vec3f(-1.0, 0.0, -1.0),
		  compute::int4_(s, s, s,0),
		  vec3f(m_particleRadius*2.0f),
		  jitter,
		  vec3f(0.0),
		  m_numParticles,
		  100.0);
      }
      break;
   }

   //copy the generated value to the device - all the contents
   m_pos->copy(GpuArray<compute::float4_>::HOST_TO_DEVICE);
   m_vel->copy(GpuArray<compute::float4_>::HOST_TO_DEVICE);   
}


void ParticleSystem::initCubeRandom(vec3f origin,vec3f size,vec3f vel,float lifetime)
{

   //get the reference to the host position and velocity
   std::vector<compute::float4_> &posHostRef = m_pos->getHostBuffer();
   std::vector<compute::float4_> &velHostRef = m_vel->getHostBuffer();

   assert(posHostRef.size() == m_numParticles);
   assert(velHostRef.size() == m_numParticles);
   
   for(int i = 0; i < m_numParticles; i++)
   {
      vec3f pos = origin + svrand() * size;

      compute::float4_ posValue(pos.x, pos.y, pos.z,0.0f);
      compute::float4_ velValue(vel.x,vel.y,vel.z,lifetime);

      //initialize the random position and velocity value
      posHostRef[i] = posValue;
      velHostRef[i] = velValue;
   }
}


