#ifndef SIM_PARAMS_H
#define SIM_PARAMS_H

#include "nvVector.h"
#include "nvMatrix.h"

typedef struct SimParams
{
   //compute::float4_  gravity;
   cl_float4 gravity;
   
   float globalDamping;
   float noiseFreq;
   float noiseAmp;
   
   //compute::float4_ cursorPos;
   cl_float4 cursorPos;
   
   float time;
   
   //compute::float4_ noiseSpeed;
   cl_float4 noiseSpeed;
} SimParams;

struct float4x4
{
    float m[16];
};

#endif
