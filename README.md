# README #

This is a re-engineered OpenCL project inspired from CUDA.


* Quick summary

  This project demonstrate the volumetric particle shadows in OpenCL. It only requires a single 2D shadow texture and the ability to sort the particles along the given axis.



* To compile and run the application the user has to have an OpenCL enabled graphics driver. This project was developed on NVIDIA graphics board with OpenCL 1.1 specification. The user also has to install the parallel primitive library Boost Compute (https://github.com/kylelutz/compute)
* A tiny Makefile is included in the source. Change the path to the boost library where it is installed locally and run make. Only tested on Ubuntu Linux 14.04

* The GLSL shader part is fully adopted from the CUDA SDK. The user should inspect the following list of files if they are interested about the GPGPU programming with OpenCL:
   - GpuArray.h
   - ParticleSystem.h && ParticleSystem.cpp
   - SharedContextQueue.h && SharedContextQueue.h 